FROM lorisleiva/laravel-docker:8.2

# Set working directory
WORKDIR /var/www

# Add grpc extension
RUN apk add grpc

# Increase Upload Max File Size
COPY ./custom.ini /usr/local/etc/php/conf.d/custom.ini

# Copy application files
COPY . /var/www

# Ensure permissions are set correctly
RUN chown -R www-data:www-data /var/www \
    && chmod -R 775 /var/www/storage /var/www/bootstrap/cache

# Remove existing composer.lock if present and install dependencies
RUN rm -f composer.lock \
    && composer install --no-dev --optimize-autoloader

# Clear caches
RUN php artisan cache:clear \
    && php artisan config:clear

# Migration
# RUN php artisan migrate:fresh --seed

# Optional environment setup
# COPY .env.example .env
# RUN php artisan key:generate

# Expose the port the app runs on
EXPOSE 8000

# Start Laravel server
CMD ["php", "artisan", "serve", "--host=0.0.0.0", "--port=8000"]
