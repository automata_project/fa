<?php

namespace App\Http\Controllers;
use App\Models\History;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;


class UserController extends Controller
{
    public function index()
    {
        // Retrieve all users from the database
        // Get the currently authenticated user
        $users = Auth::user();

        if(!$users){
            return view('profileGuest');
        }


        // dd($users);
        // $users = User::all();
        // dd($users);
        $history = History::where('user_id', $users->id)->get();
       
   
        // dd($history);
        // Pass the users data to the view
        return view('profile', ['history' => $history], compact('users'));
    }
}
