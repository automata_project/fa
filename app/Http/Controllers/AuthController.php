<?php

namespace App\Http\Controllers;

use Illuminate\Console\View\Components\Alert;
use Illuminate\Http\Request;
use Hash;
use Psy\Readline\Hoa\Console;
use Session;
use App\Models\User;
use Illuminate\Support\Facades\Auth;


class AuthController 
{

    public function customLogin(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
        // dd($request);

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return view('automata');
        }
        // $validator['emailPassword'] = 'Email address or password is incorrect.';
        // return redirect("users.login")->withErrors($validator);

        // Attempt to log the user in
        $user = User::where('email', $request->email)->first();

        if ($user && Hash::check($request->password, $user->password)) {
            // Log the user in
            Auth::login($user);

            // Redirect to the intended page
            return view('automata');
        }

        // If unsuccessful, redirect back with errors
        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ]);


    }





    public function customRegistration(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'photo' => 'required',
        ]);

       // Handle the file upload
     
        $file = $request->file('photo');
        $path = $file->store('uploads', 'public'); // Store the file in the 'public/uploads' directory
     
        // Optionally, save the path to the database
        // YourModel::create(['file_path' => $path]);

        // dd($request->all()) ;
        $check = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'photo' => $path
        ]);
        
       
        // dd($check);
        Auth::login($check);

      
        return view('automata');
        }
        
    


    // public function create(array $data, $path)
    // {
    //     return User::create([
    //         'name' => $data['name'],
    //         'email' => $data['email'],
    //         'password' => Hash::make($data['password']),
    //         'photo' => $path
    //     ]);
    // }


    public function signOut()
    {
        Session::flush();
        Auth::logout();

        return view('automata');
    }

}