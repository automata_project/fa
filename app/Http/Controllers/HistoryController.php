<?php

namespace App\Http\Controllers;
use App\Models\History;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class HistoryController extends Controller
{
    function isDFAHistory(){
        $users = Auth::user();
        if(!$users){
            return view('fa.isDFA');
        }


        // dd($users);
        // $users = User::all();
        // dd($users);
        $history = History::create(
            [
                'user_id' => $users->id,
                'detail'  => "You tested if a FA is deterministic or non-deterministic."
            ]
        );
    //    dd($history);
       return view('fa.isDFA');
    }



    function NFA2DFAHistory(){
        $users = Auth::user();
        if(!$users){
            return view('fa.NFAtoDFA');
        }


        // dd($users);
        // $users = User::all();
        // dd($users);
        $history = History::create(
            [
                'user_id' => $users->id,
                'detail'  => "You constructed an equivalent DFA from a NFA"
            ]
        );
    //    dd($history);
       return view('fa.NFAtoDFA');
    }



    function stringIsAcceptedHistory(){
        $users = Auth::user();
        if(!$users){
            return view('fa.isStringAccepted');
        }


        // dd($users);
        // $users = User::all();
        // dd($users);
        $history = History::create(
            [
                'user_id' => $users->id,
                'detail'  => "You tested if a string is accepted by a FA"
            ]
        );
    //    dd($history);
       return view('fa.isStringAccepted');
    }




    function minimizeDFAHistory(){
        $users = Auth::user();
        if(!$users){
            return view('fa.minimizeDFA');
        }


        // dd($users);
        // $users = User::all();
        // dd($users);
        $history = History::create(
            [
                'user_id' => $users->id,
                'detail'  => "You minimized a DFA"
            ]
        );
    //    dd($history);
       return view('fa.minimizeDFA');
    }
}
