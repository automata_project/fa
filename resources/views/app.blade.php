<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @vite('resources/css/app.css')
    <script src="https://cdn.tailwindcss.com"></script>
    <title>Automata</title>
</head>
<body class="flex flex-col min-h-screen">

    <header class="w-full h-24 border text-white opacity-80">
        @include('header')
    </header>

    <div class="w-full">
        @include('main')
    </div>

    <footer class="mt-auto">
        @include('footer')
    </footer>

</body>
</html>