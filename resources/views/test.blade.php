<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FA Definition</title>
</head>
<body>
    <h1>Define Finite Automaton</h1>
    <form id="faForm">
        <h2>Initial State</h2>
        <label for="initialState">Initial State:</label>
        <input type="text" id="initialState" name="initialState" required><br>
        
        <h2>Accept States</h2>
        <label for="acceptState">Accept State:</label>
        <input type="text" id="acceptState" name="acceptState">
        <button type="button" onclick="addAcceptState()">Add Accept State</button><br>
        <ul id="acceptStatesList"></ul>
        
        <h2>Add State and Transitions</h2>
        <label for="state">State:</label>
        <input type="text" id="state" name="state" required><br>
        
        <label for="transition0">Transition for '0' (format: state):</label>
        <input type="text" id="transition0" name="transition0"><br>
        
        <label for="transition1">Transition for '1' (format: state):</label>
        <input type="text" id="transition1" name="transition1"><br>
        
        <button type="button" onclick="addState()">Add State</button><br><br>
        <button type="submit">Generate JSON</button>
    </form>
    
    <h2>FA JSON Representation</h2>
    <pre id="faJson"></pre>





<script >



let fa = {
  states: {},
  initialState: null,
  acceptStates: []
};
const fatest = {
    'q0': {
        '0': ['q1'], 
        '1': ['q0']
    },
    'q1': {
        '0': ['q0'],
        '1': ['q1']
    }
  };
fa.states =  fatest;
fa =  JSON.stringify(fa, null, 2);
console.log(fa);
{
  "states": {
    "q0": {
      "0": [
        "q1"
      ],
      "1": [
        "q0"
      ]
    },
    "q1": {
      "0": [
        "q0"
      ],
      "1": [
        "q1"
      ]
    }
  },
  "initialState": null,
  "acceptStates": []
}
// fa.initialState = initialStateInput;
// // Function to add a state and its transitions to the FA
// function addState() {
//   const state = document.getElementById('state').value.trim();
//   const transition0 = document.getElementById('transition0').value.trim();
//   const transition1 = document.getElementById('transition1').value.trim();

//   if (state === '') {
//     alert('State name cannot be empty');
//     return;
//   }

//   if (!fa.states[state]) {
//     fa.states[state] = {};
//   }
//   if (transition0 !== '') {
//     fa.states[state]['0'] = transition0;
//   }
//   if (transition1 !== '') {
//     fa.states[state]['1'] = transition1;
//   }

//   // Clear input fields
//   document.getElementById('state').value = '';
//   document.getElementById('transition0').value = '';
//   document.getElementById('transition1').value = '';
// }

// // Function to add an accept state
// function addAcceptState() {
//   const acceptState = document.getElementById('acceptState').value.trim();
//   if (acceptState !== '' && !fa.acceptStates.includes(acceptState)) {
//     fa.acceptStates.push(acceptState);

//     const li = document.createElement('li');
//     li.textContent = acceptState;
//     document.getElementById('acceptStatesList').appendChild(li);

//     document.getElementById('acceptState').value = '';
//   }
// }

// // Function to load the FA and generate JSON
// function generateJson(event) {
//   event.preventDefault();

//   const initialStateInput = document.getElementById('initialState').value.trim();
//   if (initialStateInput === '') {
//     alert('Initial state cannot be empty');
//     return;
//   }

//   fa.initialState = initialStateInput;

//   document.getElementById('faJson').textContent = JSON.stringify(fa, null, 2);
// }

// Event listener for form submission to generate the JSON
document.getElementById('faForm').addEventListener('submit', generateJson);

    </script>
</body>
</html>
