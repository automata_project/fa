@extends('app')

@section('content')
<div class="flex flex-col md:flex-row w-3/4 bg-white rounded-lg  overflow-hidden m-auto max-w-[1000px]">
        
        <!-- Sign In Section -->
        <div class="w-1/2 p-8 m-auto font-serif">
            <h2 class="text-2xl font-bold mb-6 text-center">Welcome Back!</h2>
            <p class="text-center mb-6">Please enter your details</p>
            <form action="{{ route('login.custom') }}" method="POST">
                            @csrf
           
            <div class="mt-4">
                <label for="text">Email <span class="text-red-600">*</span></label>
                <input type="email" id="email" name="email" class="mt-2 p-2 w-full border rounded-md focus:outline-none focus:ring-2 focus:ring-black focus:border-transparent" placeholder="Email">
            </div>
            <div class="mt-4">
                <label for="text">Password <span class="text-red-600">*</span></label>
                <input type="password" id="password" name="password" class="mt-2 p-2 w-full border rounded-md focus:outline-none focus:ring-2 focus:ring-black focus:border-transparent" placeholder="Password">
            </div>
          
            <button id="signinBtn" class="mt-4 w-full py-2 bg-blue-500 text-white rounded-md hover:bg-blue-600">Sign In</button>
            </form>
            <p class="text-center mt-6">
                 Haven't acccount ? <a href="{{route('users.register')}}" class="text-blue-500 hover:underline">Sign up</a>
            </p>
        </div>
    </div>
    <script>
  

    </script>
@endsection






