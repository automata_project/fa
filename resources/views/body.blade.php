<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Finite Automaton Designer</title>
    <script src="https://cdn.tailwindcss.com"></script>

</head>
<body class="bg-gray-100">

    <!-- Navigation Bar -->
    <nav class="bg-white shadow-md">
        <div class="max-w-7xl mx-auto px-2 sm:px-6 lg:px-8">
            <div class="relative flex items-center justify-between h-16">
                <div class="absolute inset-y-0 left-0 flex items-center">
                    <img src="app-logo.png" alt="App Logo" class="h-12 w-12 object-cover mr-4 bg-blue-700"> <!-- Adjust path to your logo -->
                    <div class="flex space-x-4">
                        <a href="body.html" class="text-black font-medium px-3 py-2 rounded-md text-2xl border">Design FA</a>
                        <a href="body1.html" class="text-black font-medium px-3 py-2 rounded-md text-2xl border">Test FA</a>
                        <a href="" class="text-black font-medium px-3 py-2 rounded-md text-2xl border">Test String</a>
                        <a href="" class="text-black font-medium px-3 py-2 rounded-md text-2xl border">Construct DFA</a>
                        <a href="" class="text-black font-medium px-3 py-2 rounded-md text-2xl border">Minimize DFA</a>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <!-- Main Content -->
    <div class="max-w-4xl mx-auto p-6 mt-4">
        <div class="bg-gray-300 p-6 rounded-md shadow-md">
            <h1 class="text-3xl font-bold mb-4">Design a finite automaton (FA)</h1>
            <label class="block text-gray-700 text-xl font-bold mb-2" for="alphabet">
                Alphabet:
            </label>
            <input type="text" id="alphabet" name="alphabet" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight text-2xl focus:outline-none focus:shadow-outline">
        </div>
    </div>

</body>
</html>
