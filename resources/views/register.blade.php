@extends('app')

@section('content')
<div class="flex flex-col md:flex-row w-full bg-white rounded-lg shadow-lg overflow-hidden m-auto max-w-[1000px]">
    <!-- logo -->
    <!-- <div class="md:w-1/2 bg-green-600 flex items-center justify-center p-8">
            
        </div> -->
    <!-- Sign Up Section -->
    <div class="m-auto p-8">
        <h2 class="text-2xl font-bold mb-6 text-center font-serif">Create Account</h2>
        <p class="text-center mb-6 font-light">Please fill in the details</p>
        <form action="{{ route('register.custom') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div onclick="document.getElementById('photo').click();" class="flex flex-col justify-center items-center cursor-pointer">
                <img class="rounded-full border-2 object-cover shadow-lg border-slate-600" id="image-preview" src="{{ asset('uploads/defaultImage.png') }}"
                style="height: 150px; width:150px; " >
                <p class="font-serif">Choose your photo<span class="text-red-600">*</span></p>
            </div>
            
            <div class="mt-6 ">
             
                <input type="file" id="photo" name="photo" onchange="previewImage(event)"
                    class="mt-2 p-2 w-full border rounded-md focus:outline-none focus:ring-2 focus:ring-black focus:border-transparent hidden">
            </div>

            <div class="mt-6">
                <label for="text" class="font-serif">Your name<span class="text-red-600">*</span></label>
                <input type="text" id="name" name="name"
                    class="mt-2 p-2 w-full border rounded-md focus:outline-none focus:ring-2 focus:ring-black focus:border-transparent"
                    placeholder="Username">
            </div>
            <div class="mt-6">
                <label for="text" class="font-serif">Your e-mail<span class="text-red-600">*</span></label>
                <input type="email" id="email" name="email"
                    class="mt-2 p-2 w-full border rounded-md focus:outline-none focus:ring-2 focus:ring-black focus:border-transparent"
                    placeholder="Email">
            </div>
            <div class="mt-6">
                <label for="text" class="font-serif">Your password<span class="text-red-600">*</span></label>
                <input type="password" id="password" name="password"
                    class="mt-2 p-2 w-full border rounded-md focus:outline-none focus:ring-2 focus:ring-black focus:border-transparent"
                    placeholder="Password">
            </div>
            <!-- <div class="mt-6 grid grid-cols-2">
                <input id="age" class="mt-2 p-2 mr-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-black focus:border-transparent" placeholder="Age">
                <select id="gender" class="mt-2 ml-2 pl-2 border rounded-md focus:outline-none focus:ring-2 focus:ring-black focus:border-transparent">
                    <option value="" disabled selected>Select Gender</option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                    <option value="other">Other</option>
                </select>
            </div> -->
            <!-- <div class="">
                <button type="submit" class="p-4 bg-slate-200 rounded-lg">Sign up</button>
            </div> -->

            <button type="submit" id="signupBtn"
                class="mt-4 w-full py-2 bg-green-500 text-white rounded-md hover:bg-green-600 font-serif">Sign Up</button>
        </form>
        <p class="text-center mt-6">
            Already have an account? <a href="{{route('users.login')}}" class="text-blue-500 hover:underline font-light">Sign
                In</a>
        </p>
    </div>
</div>
<script>
    function previewImage(event) {
        const file = event.target;
        const imagePreview = document.getElementById('image-preview');

        if (file.files && file.files[0]) {
            const reader = new FileReader();

            reader.onload = function (e) {
                imagePreview.src = e.target.result;
            };

            reader.readAsDataURL(file.files[0]);
        }
    }
</script>
@endsection