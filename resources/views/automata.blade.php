@extends('app')
@section('content')
    <div class="grid grid-cols-1 lg:grid-cols-2 gap-4 m-auto  w-3/4 mt-8">
        <div class="flex flex-col text-lg cursor-pointer bg-blue-300 text-black font-bold p-4 rounded-lg border w-full max-w-[800px] h-52 justify-self-center hover:shadow-lg hover:-translate-y-1 transition hover:delay-100 ">
              <h1>Test if a FA is deterministic or non-deterministic</h1>
              <a href="{{route('history.isDFA')}}" class="mt-auto ml-auto rounded-full w-14 h-14 bg-slate-300 py-2 px-4 hover:text-white hover:bg-black shadow-md text-center ">
                <p class="-translate-x-2 translate-y-1">Start</p>
              </a>
        </div>
        <div class="flex flex-col text-lg cursor-pointer bg-teal-300 text-black font-bold p-4 rounded-lg border w-full max-w-[800px] h-52 justify-self-center hover:shadow-lg hover:-translate-y-1 transition hover:delay-100 ">
            <h1>Test if a string is accepted by a FA</h1>
            <a href="{{route('history.stringIsAccepted')}}" class="mt-auto ml-auto rounded-full w-14 h-14 bg-slate-300 py-2 px-4 hover:text-white hover:bg-black shadow-md text-center ">
              <p class="-translate-x-2 translate-y-1">Start</p>
            </a>
        </div>
        <div class="flex flex-col text-lg cursor-pointer bg-pink-300 text-black font-bold p-4 rounded-lg border w-full max-w-[800px] h-52 justify-self-center hover:shadow-lg hover:-translate-y-1 transition hover:delay-100 ">
            <h1>Construct an equivalent DFA from an NFA</h1>
            <a href="{{route('history.NFA2DFA')}}" class="mt-auto ml-auto rounded-full w-14 h-14 bg-slate-300 py-2 px-4 hover:text-white hover:bg-black shadow-md text-center ">
              <p class="-translate-x-2 translate-y-1">Start</p>
            </a>
        </div>
        <div class="flex flex-col text-lg cursor-pointer bg-red-300 text-black font-bold p-4 rounded-lg border w-full max-w-[800px] h-52 justify-self-center hover:shadow-lg hover:-translate-y-1 transition hover:delay-100 ">
            <h1>Minimize a DFA</h1>
            <a href="{{route('history.minimizeDFA')}}" class="mt-auto ml-auto rounded-full w-14 h-14 bg-slate-300 py-2 px-4 hover:text-white hover:bg-black shadow-md text-center ">
              <p class="-translate-x-2 translate-y-1 ">Start</p>
            </a>
        </div>

    </div>

       

@endsection