
<div class="header flex flex-col justify-center h-full w-full font-serif">
    <div class="h-full flex justify-evenly border bg-slate-800 items-center text-white w-full min-w-[860px]">
        <div class="w-1/3 flex">
            <div class="ml-4 text-sm lg:text-lg cursor-pointer  text-black font-bold p-4  justify-self-center">
                <a href="{{route('users.profile')}}">
                <img class="rounded-full border-2 object-cover " src="{{ asset('uploads/defaultPerson.png') }}"
                style="height: 70px; width:70px; " >
                </a>   
            </div>
        </div>
        
        <div class="flex w-1/3 justify-evenly">
            <div class="text-sm lg:text-lg cursor-pointer bg-slate-100 text-black font-bold p-4 rounded-lg border">
                <a href="{{route('users.automata')}}">Automata</a>
            </div>
            <div class="text-sm lg:text-lg cursor-pointer bg-slate-100 text-black font-bold p-4 rounded-lg border">
                <a href="{{route('admin.tutorial')}}">Tutorial</a>
            </div>
            <div class="text-sm lg:text-lg cursor-pointer bg-slate-100 text-black font-bold p-4 rounded-lg border">
                <a href="{{route('admin.team')}}">Our Team</a>
            </div>
        </div>
       <div class="flex  w-1/3 justify-end">
            <div class="mr-4 text-sm lg:text-lg cursor-pointer bg-slate-100 text-black font-bold p-4 rounded-lg border ">
                <a href="{{route('users.register')}}">Register</a>
            </div>
            <div class="mr-4 text-sm lg:text-lg cursor-pointer bg-slate-100 text-black font-bold p-4 rounded-lg border ">
                <a href="{{route('users.login')}}">Login</a>
            </div>
       </div>
        

    </div>
</div>