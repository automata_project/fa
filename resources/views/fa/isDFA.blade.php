@extends('app')


@section('content')


<h2 class="w-full text-center py-4 font-bold text-xl font-serif">You can test your FA whether it is deterministic or non-deterministic.</h2>
<div class="w-full flex flex-col justify-center items-center">
  <div class="container w-full flex justify-center ">
    <label for="numStates" class=" bg-slate-300 p-3 border-2 rounded-lg font-mono font-semibold ">Number of States:</label>
    <input type="number" id="numStates" min="1" value="2" class="font-semibold bg-slate-100 outline-none w-24 rounded-lg text-center">
    <label for="numSymbols"  class="ml-14 bg-slate-300 p-3 border-2 rounded-lg font-mono font-semibold">Number of Symbols:</label>
    <input type="number" id="numSymbols" min="1" value="2" class="font-semibold bg-slate-100 outline-none w-24 rounded-lg text-center">
    <button type="button" onclick="generateForm()"  class="ml-14 bg-blue-300 p-3 border-2 rounded-lg font-bold ">Generate Form</button>
</div>

<form id="stateForm" class="grid grid-cols-2 gap-12 mt-8">

</form>
</div>

  <div class="mt-10  w-full flex justify-center p-4">
      <button type="button" onclick="createJSON()" class="bg-sky-300 p-4 rounded-lg shadow-lg font-mono font-semibold" >Check Your FA</button>
  </div>
  

<div class="result w-3/4 flex m-auto min-h-80 mb-8 border-2 rounded-lg">
  <div class="w-1/2 border-r-2 bg-blue-100 p-2 font-mono font-semibold pl-4">
    <h3 class="text-xl font-extrabold">Generated JSON:</h3>
    <pre id="jsonOutput">Your FA will be generated in JSON format here!</pre>
  </div>
    
  <div class="w-1/2 bg-sky-100 p-2 font-mono font-semibold pl-4">
    <h3 class="text-xl font-extrabold">Result:</h3>
    <h2 id="display">Your FA result will be displayed here!</h2>
  </div>
    
</div>

<script>



    function generateForm() {
        const numStates = document.getElementById('numStates').value;
        const numSymbols = document.getElementById('numSymbols').value;
        const formContainer = document.getElementById('stateForm');
        formContainer.innerHTML = '';

        for (let i = 0; i < numStates; i++) {
            const stateDiv = document.createElement('div');
            stateDiv.className = 'container';
            stateDiv.innerHTML = `<h3 class="mt-4 bg-slate-300 p-3 border-2 rounded-lg font-mono font-semibold">State q${i}</h3>`;

            for (let j = 0; j < numSymbols; j++) {
                const inputGroup = document.createElement('div');
                inputGroup.className = 'input-group';
                inputGroup.innerHTML = `
                    <label for="q${i}-${j}" class="bg-slate-300 h-10 border-2 rounded-lg  p-3 ">${j}:</label> 
                    <input type="text" id="q${i}-${j}" placeholder="q1,q0" class="mt-2 bg-slate-100 p-3 border-2 rounded-lg font-mono font-semibold ">
                `;
                stateDiv.appendChild(inputGroup);
            }

            formContainer.appendChild(stateDiv);
        }
    }

    function createJSON() {
        const numStates = document.getElementById('numStates').value;
        const numSymbols = document.getElementById('numSymbols').value;
        const jsonObj = {};

        for (let i = 0; i < numStates; i++) {
            const stateObj = {};

            for (let j = 0; j < numSymbols; j++) {
                const input = document.getElementById(`q${i}-${j}`).value.split(',').map(v => v.trim());
                stateObj[j] = input;
            }

            jsonObj[`q${i}`] = stateObj;
        }

        resultJSON = JSON.stringify(jsonObj, null, 4);
        document.getElementById('jsonOutput').textContent = resultJSON;
        submitJSON();
    }



    //testing
    function submitJSON(){

    result = submitJson();
    display =document.getElementById('display');
    if(result == true){
        display.innerHTML = `
        <p class="text-green-600">True</p>
        <p>Your FA is is deterministic</p>
        `;
        
    }else{
      display.innerHTML = `
        <p class="text-red-600">False</p>
        <p>Your FA is is non-deterministic</p>
        `;
    }


    function submitJson() { 
    // fa =document.getElementById('dfa_test').value.trim();
    // fa = JSON.parse(fa);
    // console.log(fa);
    // console.log("JSON? : ",validateJson(fa));
    // isDeterministicFA(fa);

    fa = resultJSON;
    fa = JSON.parse(fa);
    // console.log(fa);
    for (let state in fa) {
      if (fa.hasOwnProperty(state)) {
        let transitions = fa[state];
        for (let symbol in transitions) {
          if (transitions.hasOwnProperty(symbol)) {
            // If there's more than one transition for a symbol, it's non-deterministic
            if (transitions[symbol].length > 1) {
              return false;
            }
          }
        }
      }
    }
    return true;
    
  }


}

    document.addEventListener("DOMContentLoaded", () => {
      generateForm();
      });
</script>

@endsection