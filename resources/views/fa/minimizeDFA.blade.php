@extends('app')


@section('content')



<h2 class="w-full text-center py-4 font-bold text-xl font-serif">The page for minimizing DFA</h2>
<div class="w-full flex flex-col justify-center items-center">
    <div class="flex w-full justify-center my-4 ">

        <label for="initialState" class=" bg-slate-300 p-3 border-2 rounded-lg font-mono font-semibold ">Start
            State:</label>
        <input type="text" id="initialState" name="initialState" placeholder="q0"
            class="font-semibold bg-slate-100 outline-none w-24 rounded-lg text-center" required><br>


        <label for="acceptState" class="ml-8 bg-slate-300 p-3 border-2 rounded-lg font-mono font-semibold ">Final
            State(s):</label>
        <input type="text" id="acceptState" name="acceptState" placeholder="q0, q1"
            class="font-semibold bg-slate-100 outline-none w-24 rounded-lg text-center" required>
        <!-- <button type="button" onclick="addAcceptState()" class="ml-14 bg-blue-300 p-3 border-2 rounded-lg font-bold ">Add Accept State</button><br>
        <ul id="acceptStatesList"></ul> -->
    </div>


    <div class="container w-full flex justify-center ">
        <label for="numStates" class=" bg-slate-300 p-3 border-2 rounded-lg font-mono font-semibold ">Number of
            States:</label>
        <input type="number" id="numStates" min="1" value="2"
            class="font-semibold bg-slate-100 outline-none w-24 rounded-lg text-center">
        <label for="numSymbols" class="ml-14 bg-slate-300 p-3 border-2 rounded-lg font-mono font-semibold">Number of
            Symbols:</label>
        <input type="number" id="numSymbols" min="1" value="2"
            class="font-semibold bg-slate-100 outline-none w-24 rounded-lg text-center">
        <button type="button" onclick="generateForm()"
            class="ml-14 bg-blue-300 p-3 border-2 rounded-lg font-bold ">Generate Form</button>
    </div>

    <form id="stateForm" class="grid grid-cols-2 gap-12 mt-8">

    </form>
</div>

<div class="mt-10  w-full flex flex-col justify-center p-4 items-center">
   
    <button type="button" onclick="createJSON()"
        class="bg-sky-300 p-4 rounded-lg shadow-lg font-mono font-semibold w-48">Minimize Your DFA</button>
</div>


<div class="result w-3/4 flex m-auto min-h-80 mb-8 border-2 rounded-lg">
  <div class="w-1/2 border-r-2 bg-blue-100 p-2 font-mono font-semibold pl-4">
    <h3 class="text-xl font-extrabold">Generated JSON:</h3>
    <pre id="jsonOutput">Your initial DFA will be generated in JSON format here!</pre>
  </div>
    
  <div class="w-1/2 bg-sky-100 p-2 font-mono font-semibold pl-4">
    <h3 class="text-xl font-extrabold">Result:</h3>
    <pre id="display">Your minimized DFA result will be displayed here!</pre>
  </div>
    
</div>



<script>

let fa = {
        states: {},
        initialState: null,
        acceptStates: []
    };

    function generateForm() {
        const numStates = document.getElementById('numStates').value;
        const numSymbols = document.getElementById('numSymbols').value;
        const formContainer = document.getElementById('stateForm');
        formContainer.innerHTML = '';

        for (let i = 0; i < numStates; i++) {
            const stateDiv = document.createElement('div');
            stateDiv.className = 'container';
            stateDiv.innerHTML = `<h3 class="mt-4 bg-slate-300 p-3 border-2 rounded-lg font-mono font-semibold">State q${i}</h3>`;

            for (let j = 0; j < numSymbols; j++) {
                const inputGroup = document.createElement('div');
                inputGroup.className = 'input-group';
                inputGroup.innerHTML = `
                    <label for="q${i}-${j}" class="bg-slate-300 h-10 border-2 rounded-lg  p-3 ">${j}:</label> 
                    <input type="text" id="q${i}-${j}" placeholder="q1,q0" class="mt-2 bg-slate-100 p-3 border-2 rounded-lg font-mono font-semibold ">
                `;
                stateDiv.appendChild(inputGroup);
            }

            formContainer.appendChild(stateDiv);
        }
    }

    function createJSON() {
        const numStates = document.getElementById('numStates').value;
        const numSymbols = document.getElementById('numSymbols').value;
        const jsonObj = {};

        for (let i = 0; i < numStates; i++) {
            const stateObj = {};

            for (let j = 0; j < numSymbols; j++) {
                const input = document.getElementById(`q${i}-${j}`).value.split(',').map(v => v.trim());
                stateObj[j] = input;
            }

            jsonObj[`q${i}`] = stateObj;
        }
        // console.log( jsonObj);
        // Display the JSON object
        // stateJSON = JSON.stringify(jsonObj, null, 2);
        fa.states = jsonObj;
        const acceptedStateInput = document.getElementById('acceptState').value.split(',').map(v => v.trim());
        // console.log("Acc: ",acceptedStateInput);
        const initialStateInput = document.getElementById('initialState').value.trim();
        if (initialStateInput === '') {
            alert('Initial state cannot be empty');
            return;
        }
        if (acceptedStateInput === '') {
            alert('acceptedStateInput cannot be empty');
            return;
        }
        // fa = JSON.stringify(fa, null, 2);
        fa.acceptStates = acceptedStateInput;
        fa.initialState = initialStateInput;
        // console.log("first fa: ",fa);
        // console.log("first init: ",fa.initialState);
        faDisplay = JSON.stringify(fa, null, 4);
        console.log(faDisplay);
        document.getElementById('jsonOutput').textContent = faDisplay;
        submitJSON();
    }



    //testing
    function submitJSON(){

        const minimizedDFA = minimizeDFA(fa);
        console.log("DFA2: ", minimizedDFA);
        console.log('Minimized DFA:', JSON.stringify(minimizedDFA, null, 2));
        document.getElementById('display').textContent = JSON.stringify(minimizedDFA, null, 2);
   
    
  }




    document.addEventListener("DOMContentLoaded", () => {
      generateForm();
      });





    // Define the DFA in the given format
// const dfa = {
//     states: {
//       "q0": {
//         "0": ["q1"],
//         "1": ["q0"]
//       },
//       "q1": {
//         "0": ["q0"],
//         "1": ["q1"]
//       },
//       "q2": {
//         "0": ["q2"],
//         "1": ["q1"]
//       }
//     },
//     initialState: "q0",
//     acceptStates: ["q1"]
//   };
  
  // Minimize the DFA
  function minimizeDFA(dfa) {
    // Step 1: Remove unreachable states
    const reachableStates = new Set([dfa.initialState]);
    let queue = [dfa.initialState];
  
    while (queue.length > 0) {
      const state = queue.shift();
      Object.keys(dfa.states[state]).forEach(symbol => {
        dfa.states[state][symbol].forEach(nextState => {
          if (!reachableStates.has(nextState)) {
            reachableStates.add(nextState);
            queue.push(nextState);
          }
        });
      });
    }
  
    // Filter out unreachable states
    Object.keys(dfa.states).forEach(state => {
      if (!reachableStates.has(state)) {
        delete dfa.states[state];
      }
    });
  
    // Step 2: Create partitions for equivalent states
    let partitions = [dfa.acceptStates, Object.keys(dfa.states).filter(s => !dfa.acceptStates.includes(s))];
    
    let updatedPartitions;
    do {
      updatedPartitions = [];
      partitions.forEach(partition => {
        const [firstState] = partition;
        const newPartition = [firstState];
        const restStates = partition.slice(1);
  
        restStates.forEach(state => {
          if (Object.keys(dfa.states[firstState]).every(symbol => 
            partitions.findIndex(p => p.includes(dfa.states[firstState][symbol][0])) ===
            partitions.findIndex(p => p.includes(dfa.states[state][symbol][0]))
          )) {
            newPartition.push(state);
          } else {
            updatedPartitions.push([state]);
          }
        });
  
        updatedPartitions.push(newPartition);
      });
      
      if (updatedPartitions.length === partitions.length) break;
      
      partitions = updatedPartitions;
    } while (true);
  
    // Step 3: Build the minimized DFA
    const minimizedDFA = {
      states: {},
      initialState: null,
      acceptStates: []
    };
  
    partitions.forEach(partition => {
      const stateName = partition.join(',');
      minimizedDFA.states[stateName] = {};
  
      if (partition.includes(dfa.initialState)) {
        minimizedDFA.initialState = stateName;
      }
      
      if (partition.some(state => dfa.acceptStates.includes(state))) {
        minimizedDFA.acceptStates.push(stateName);
      }
      
      Object.keys(dfa.states[partition[0]]).forEach(symbol => {
        const representativeState = partition[0];
        const nextState = dfa.states[representativeState][symbol][0];
        const nextStatePartition = partitions.find(p => p.includes(nextState));
        minimizedDFA.states[stateName][symbol] = [nextStatePartition.join(',')];
      });
    });
  
    return minimizedDFA;
  }
  
  
</script>


@endsection