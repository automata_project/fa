@extends('app')


@section('content')


<h2 class="w-full text-center py-4 font-bold text-xl font-serif">The page for check if string is accepted.</h2>
<div class="w-full flex flex-col justify-center items-center">
    <div class="flex w-full justify-center my-4 ">

        <label for="initialState" class=" bg-slate-300 p-3 border-2 rounded-lg font-mono font-semibold ">Start
            State:</label>
        <input type="text" id="initialState" name="initialState" placeholder="q0"
            class="font-semibold bg-slate-100 outline-none w-24 rounded-lg text-center" required><br>


        <label for="acceptState" class="ml-8 bg-slate-300 p-3 border-2 rounded-lg font-mono font-semibold ">Final
            State(s):</label>
        <input type="text" id="acceptState" name="acceptState" placeholder="q0, q1"
            class="font-semibold bg-slate-100 outline-none w-24 rounded-lg text-center" required>
        <!-- <button type="button" onclick="addAcceptState()" class="ml-14 bg-blue-300 p-3 border-2 rounded-lg font-bold ">Add Accept State</button><br>
        <ul id="acceptStatesList"></ul> -->
    </div>


    <div class="container w-full flex justify-center ">
        <label for="numStates" class=" bg-slate-300 p-3 border-2 rounded-lg font-mono font-semibold ">Number of
            States:</label>
        <input type="number" id="numStates" min="1" value="2"
            class="font-semibold bg-slate-100 outline-none w-24 rounded-lg text-center">
        <label for="numSymbols" class="ml-14 bg-slate-300 p-3 border-2 rounded-lg font-mono font-semibold">Number of
            Symbols:</label>
        <input type="number" id="numSymbols" min="1" value="2"
            class="font-semibold bg-slate-100 outline-none w-24 rounded-lg text-center">
        <button type="button" onclick="generateForm()"
            class="ml-14 bg-blue-300 p-3 border-2 rounded-lg font-bold ">Generate Form</button>
    </div>

    <form id="stateForm" class="grid grid-cols-2 gap-12 mt-8">

    </form>
</div>

<div class="mt-10  w-full flex flex-col justify-center p-4 items-center">
    <input type="text" id="addString" name="addString"
        class="p-4 my-4 bg-slate-100 text-center border-2 border-blue-400 outline-emerald-500 rounded-lg"
        placeholder="Add your string">
    <button type="button" onclick="createJSON()"
        class="bg-sky-300 p-4 rounded-lg shadow-lg font-mono font-semibold w-48">Check Your String</button>
</div>
<!-- <br>
<div id="jsonOutput"></div>
<br>
<div id="display"></div> -->

<div class="result w-3/4 flex m-auto min-h-80 mb-8 border-2 rounded-lg">
  <div class="w-1/2 border-r-2 bg-blue-100 p-2 font-mono font-semibold pl-4">
    <h3 class="text-xl font-extrabold">Generated JSON:</h3>
    <pre id="jsonOutput">Your FA will be generated in JSON format here!</pre>
  </div>
    
  <div class="w-1/2 bg-sky-100 p-2 font-mono font-semibold pl-4">
    <h3 class="text-xl font-extrabold">Result:</h3>
    <h2 id="display">Your FA result will be displayed here!</h2>
  </div>
    
</div>


<script>

    let fa = {
        states: {},
        initialState: null,
        acceptStates: []
    };

    function generateForm() {
        const numStates = document.getElementById('numStates').value;
        const numSymbols = document.getElementById('numSymbols').value;
        const formContainer = document.getElementById('stateForm');
        formContainer.innerHTML = '';

        for (let i = 0; i < numStates; i++) {
            const stateDiv = document.createElement('div');
            stateDiv.className = 'container';
            stateDiv.innerHTML = `<h3 class="mt-4 bg-slate-300 p-3 border-2 rounded-lg font-mono font-semibold">State q${i}</h3>`;

            for (let j = 0; j < numSymbols; j++) {
                const inputGroup = document.createElement('div');
                inputGroup.className = 'input-group';
                inputGroup.innerHTML = `
                    <label for="q${i}-${j}" class="bg-slate-300 h-10 border-2 rounded-lg  p-3 ">${j}:</label> 
                    <input type="text" id="q${i}-${j}" placeholder="q1,q0" class="mt-2 bg-slate-100 p-3 border-2 rounded-lg font-mono font-semibold ">
                `;
                stateDiv.appendChild(inputGroup);
            }

            formContainer.appendChild(stateDiv);
        }
    }

    function createJSON() {
        const numStates = document.getElementById('numStates').value;
        const numSymbols = document.getElementById('numSymbols').value;
        const jsonObj = {};

        for (let i = 0; i < numStates; i++) {
            const stateObj = {};

            for (let j = 0; j < numSymbols; j++) {
                const input = document.getElementById(`q${i}-${j}`).value.split(',').map(v => v.trim());
                stateObj[j] = input;
            }

            jsonObj[`q${i}`] = stateObj;
        }
        // console.log( jsonObj);
        // Display the JSON object
        // stateJSON = JSON.stringify(jsonObj, null, 2);
        fa.states = jsonObj;
        const acceptedStateInput = document.getElementById('acceptState').value.split(',').map(v => v.trim());
        // console.log("Acc: ",acceptedStateInput);
        const initialStateInput = document.getElementById('initialState').value.trim();
        if (initialStateInput === '') {
            alert('Initial state cannot be empty');
            return;
        }
        if (acceptedStateInput === '') {
            alert('acceptedStateInput cannot be empty');
            return;
        }
        // fa = JSON.stringify(fa, null, 2);
        fa.acceptStates = acceptedStateInput;
        fa.initialState = initialStateInput;
        // console.log("first fa: ",fa);
        // console.log("first init: ",fa.initialState);
        faDisplay = JSON.stringify(fa, null, 4);
        console.log(faDisplay);
        document.getElementById('jsonOutput').textContent = faDisplay;
        submitJSON();
    }

    // Test if a string is accepted by FA
    function isStringAcceptedByFA(inputString) {
        const faInput = fa;
        // faInput =JSON.parse(faInput);
        const StringInput = inputString;
        // Validate the structure of faInput
        if (!faInput || !faInput.initialState || !faInput.states || !faInput.acceptStates) {
            // console.error("Invalid FA input:",faInput);
            return false;
        }
        let currentState = faInput.initialState;

        // Process each symbol in the input string
        for (let i = 0; i < StringInput.length; i++) {
            let symbol = StringInput[i];

            // Check if there is a transition for the current symbol from the current state
            if (!faInput.states[currentState] || !faInput.states[currentState][symbol]) {
                // No transition for this symbol
                return false;
            }

            // Transition to the next state, assuming single state transition
            currentState = faInput.states[currentState][symbol][0];
        }

        // Check if the current state is an accept state
        return faInput.acceptStates.includes(currentState);
    }


    //testing
    function submitJSON() {

        const inputString = document.getElementById('addString').value;
        console.log("In func string: ",inputString);
        const result = isStringAcceptedByFA(inputString);
        // console.log("R: ",result);
        display = document.getElementById('display');
        if (result == true) {

            display.innerHTML = `
            <p class="text-blue-600">True</p>
            <p>Your string is Accepted !</p>
            `;
     

        } else {
            display.innerHTML = `
            <p class="text-red-600">False</p>
            <p>Your string is NOT Accepted !</p>
            `;
      
        }
    }

    document.addEventListener("DOMContentLoaded", () => {
      generateForm();
      });






        // Function to extract alphabet from transitions
    //     function getAlphabetFromTransitions(transitions) {
    //         const symbols = new Set();
    //         Object.values(transitions).forEach(stateTransitions => {
    //             Object.keys(stateTransitions).forEach(symbol => {
    //             symbols.add(symbol);
    //             });
    //         });
    //         return Array.from(symbols);
    //     }

    //   // Convert to the expected format without explicitly defining the alphabet
    //     const nfa = {
    //         states: Object.keys(providedNFA.states),
    //         alphabet: getAlphabetFromTransitions(providedNFA.states),
    //         transitions: providedNFA.states,
    //         startState: providedNFA.initialState,
    //         acceptStates: providedNFA.acceptStates
    //     };

    //     // Now you can use this `nfa` with your conversion function
    //     const dfa = convertNFAToDFA(nfa);
</script>

@endsection