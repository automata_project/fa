@extends('app')


@section('content')



<h2 class="w-full text-center py-4 font-bold text-xl font-serif">The page for converting NFA to DFA.</h2>
<div class="w-full flex flex-col justify-center items-center">
    <div class="flex w-full justify-center my-4 ">

        <label for="initialState" class=" bg-slate-300 p-3 border-2 rounded-lg font-mono font-semibold ">Start
            State:</label>
        <input type="text" id="initialState" name="initialState" placeholder="q0"
            class="font-semibold bg-slate-100 outline-none w-24 rounded-lg text-center" required><br>


        <label for="acceptState" class="ml-8 bg-slate-300 p-3 border-2 rounded-lg font-mono font-semibold ">Final
            State(s):</label>
        <input type="text" id="acceptState" name="acceptState" placeholder="q0, q1"
            class="font-semibold bg-slate-100 outline-none w-24 rounded-lg text-center" required>
        <!-- <button type="button" onclick="addAcceptState()" class="ml-14 bg-blue-300 p-3 border-2 rounded-lg font-bold ">Add Accept State</button><br>
        <ul id="acceptStatesList"></ul> -->
    </div>


    <div class="container w-full flex justify-center ">
        <label for="numStates" class=" bg-slate-300 p-3 border-2 rounded-lg font-mono font-semibold ">Number of
            States:</label>
        <input type="number" id="numStates" min="1" value="2"
            class="font-semibold bg-slate-100 outline-none w-24 rounded-lg text-center">
        <label for="numSymbols" class="ml-14 bg-slate-300 p-3 border-2 rounded-lg font-mono font-semibold">Number of
            Symbols:</label>
        <input type="number" id="numSymbols" min="1" value="2"
            class="font-semibold bg-slate-100 outline-none w-24 rounded-lg text-center">
        <button type="button" onclick="generateForm()"
            class="ml-14 bg-blue-300 p-3 border-2 rounded-lg font-bold ">Generate Form</button>
    </div>

    <form id="stateForm" class="grid grid-cols-2 gap-12 mt-8">

    </form>
</div>

<div class="mt-10  w-full flex flex-col justify-center p-4 items-center">

    <button type="button" onclick="createJSON()"
        class="bg-sky-300 p-4 rounded-lg shadow-lg font-mono font-semibold w-48">Convert NFA to DFA</button>
</div>
<!-- <br>
<div id="jsonOutput"></div>
<br>
<div id="display"></div> -->

<div class="result w-3/4 flex m-auto min-h-80 mb-8 border-2 rounded-lg">
  <div class="w-1/2 border-r-2 bg-blue-100 p-2 font-mono font-semibold pl-4">
    <h3 class="text-xl font-extrabold">Generated JSON as NFA:</h3>
    <pre id="jsonOutput">Your NFA will be generated in JSON format here!</pre>
  </div>
    
  <div class="w-1/2 bg-sky-100 p-2 font-mono font-semibold pl-4">
    <h3 class="text-xl font-extrabold">Generated JSON as DFA:</h3>
    <pre id="dfaOutput">Your DFA result will be displayed here!</pre>

  </div>
    
</div>


<script>

    let fa = {
        states: {},
        initialState: null,
        acceptStates: []
    };

    function generateForm() {
        const numStates = document.getElementById('numStates').value;
        const numSymbols = document.getElementById('numSymbols').value;
        const formContainer = document.getElementById('stateForm');
        formContainer.innerHTML = '';

        for (let i = 0; i < numStates; i++) {
            const stateDiv = document.createElement('div');
            stateDiv.className = 'container';
            stateDiv.innerHTML = `<h3 class="mt-4 bg-slate-300 p-3 border-2 rounded-lg font-mono font-semibold">State q${i}</h3>`;

            for (let j = 0; j < numSymbols; j++) {
                const inputGroup = document.createElement('div');
                inputGroup.className = 'input-group';
                inputGroup.innerHTML = `
                    <label for="q${i}-${j}" class="bg-slate-300 h-10 border-2 rounded-lg  p-3 ">${j}:</label> 
                    <input type="text" id="q${i}-${j}" placeholder="q1,q0" class="mt-2 bg-slate-100 p-3 border-2 rounded-lg font-mono font-semibold ">
                `;
                stateDiv.appendChild(inputGroup);
            }

            formContainer.appendChild(stateDiv);
        }
    }

    function createJSON() {
        const numStates = document.getElementById('numStates').value;
        const numSymbols = document.getElementById('numSymbols').value;
        const jsonObj = {};

        for (let i = 0; i < numStates; i++) {
            const stateObj = {};

            for (let j = 0; j < numSymbols; j++) {
                const input = document.getElementById(`q${i}-${j}`).value.split(',').map(v => v.trim());
                stateObj[j] = input;
            }

            jsonObj[`q${i}`] = stateObj;
        }
        // console.log( jsonObj);
        // Display the JSON object
        // stateJSON = JSON.stringify(jsonObj, null, 2);
        fa.states = jsonObj;
        const acceptedStateInput = document.getElementById('acceptState').value.split(',').map(v => v.trim());
        // console.log("Acc: ",acceptedStateInput);
        const initialStateInput = document.getElementById('initialState').value.trim();
        if (initialStateInput === '') {
            alert('Initial state cannot be empty');
            return;
        }
        if (acceptedStateInput === '') {
            alert('acceptedStateInput cannot be empty');
            return;
        }
        // fa = JSON.stringify(fa, null, 2);
        fa.acceptStates = acceptedStateInput;
        fa.initialState = initialStateInput;
        // console.log("first fa: ",fa);
        // console.log("first init: ",fa.initialState);
        faDisplay = JSON.stringify(fa, null, 4);
        // console.log(faDisplay);
        document.getElementById('jsonOutput').textContent = faDisplay;
        submitJSON();
    }

    

    //testing
    function submitJSON() {

        // const inputString = document.getElementById('addString').value;
        // // console.log("In func string: ",inputString);
        // const result = isStringAcceptedByFA(inputString);
        // // console.log("R: ",result);
        // display = document.getElementById('display');
        // if (result == true) {

        //     display.innerHTML = `
        //     <p class="text-blue-600">True</p>
        //     <p>Your string is Accepted !</p>
        //     `;
     

        // } else {
        //     display.innerHTML = `
        //     <p class="text-red-600">False</p>
        //     <p>Your string is NOT Accepted !</p>
        //     `;
      
        // }


        console.log("FA : ", fa);

        // Convert to the expected format without explicitly defining the alphabet
        const nfa = {
            states: Object.keys(fa.states),
            alphabet: getAlphabetFromTransitions(fa.states),
            transitions: fa.states,
            startState: fa.initialState,
            acceptStates: fa.acceptStates
        };
        console.log("NFA: ", nfa);


        // Convert NFA to DFA
        const dfa = convertNFAToDFA(nfa);
      

        convertSetsToArrays(dfa);
        console.log("DFA: ", dfa);

        // Remove backslashes that are escaping quotes
        // const jsonStringWithoutEscapes = dfa.replace(/\\"/g, '"');
        const jsonString = JSON.stringify(dfa);
        // Parse and format the JSON string
        const jsonObjfa = JSON.parse(jsonString);
        const convertedJsonObj = convertAutomataStates(jsonObjfa);
        // console.log(jsonObjfa);
        const formattedJsonString = JSON.stringify(convertedJsonObj, null, 4);
        console.log(formattedJsonString);
        // Apply the function to the parsed JSON object
        
    
        // console.log(faDisplay);
        document.getElementById('dfaOutput').textContent = formattedJsonString;

    }

        // Function to convert stringified array keys to actual arrays for 'states', 'startState', and 'acceptStates'
        function convertAutomataStates(obj) {
            const newObj = { ...obj }; // Shallow copy of the object

            // Convert 'states' object keys to an array of arrays
            newObj.states = Object.keys(newObj.states).map(key => JSON.parse(key));

            // Ensure 'startState' is an array
            newObj.startState = JSON.parse(newObj.startState);

            // Convert elements in 'acceptStates' array to actual arrays
            newObj.acceptStates = newObj.acceptStates.map(str => JSON.parse(str));

            return newObj;
        }

        


    function convertSetsToArrays(obj) {
        for (const key in obj) {
            if (obj.hasOwnProperty(key)) {
            const value = obj[key];
            if (value instanceof Set) {
                obj[key] = Array.from(value);
            } else if (typeof value === 'object' && value !== null) {
                convertSetsToArrays(value);
            }
            }
        }
        }
    document.addEventListener("DOMContentLoaded", () => {
      generateForm();
      });





    // NFA to DFA

        // Helper function to get the epsilon closure of a set of states
        function epsilonClosure(states, nfa) {
            let closure = new Set(states);
            let stack = [...states];

            while (stack.length > 0) {
                let state = stack.pop();
                let transitions = nfa.transitions[state] || {};
                let epsilonTransitions = transitions[""] || [];
                for (let nextState of epsilonTransitions) {
                if (!closure.has(nextState)) {
                    closure.add(nextState);
                    stack.push(nextState);
                }
                }
            }

            return closure;
        }

        // Helper function to move from a set of states with a given symbol
        function move(states, symbol, nfa) {
            let result = new Set();
            for (let state of states) {
                let transitions = nfa.transitions[state] || {};
                let symbolTransitions = transitions[symbol] || [];
                for (let nextState of symbolTransitions) {
                result.add(nextState);
                }
            }
            return result;
        }



       

        // Main function to convert NFA to DFA
        function convertNFAToDFA(nfa) {
            let dfa = {
                states: {},
                transitions: {},
                startState: null,
                acceptStates: new Set(),
            };

            let unmarkedStates = new Map([
                [
                JSON.stringify([...epsilonClosure([nfa.startState], nfa)]),
                epsilonClosure([nfa.startState], nfa),
                ],
            ]);
            dfa.startState = JSON.stringify([...unmarkedStates.values().next().value]);

            while (unmarkedStates.size > 0) {
                let currentState = unmarkedStates.keys().next().value;
                let currentStatesSet = unmarkedStates.get(currentState);
                unmarkedStates.delete(currentState);

                for (let symbol of nfa.alphabet) {
                let nextStateSet = epsilonClosure(
                    [...move(currentStatesSet, symbol, nfa)],
                    nfa
                );
                let nextStateKey = JSON.stringify([...nextStateSet]);

                if (!dfa.states[nextStateKey] && nextStateSet.size > 0) {
                    unmarkedStates.set(nextStateKey, nextStateSet);
                }

                dfa.transitions[currentState] = dfa.transitions[currentState] || {};
                dfa.transitions[currentState][symbol] = nextStateKey;
                }

                dfa.states[currentState] = currentStatesSet;
            }

            // Determine accept states for the DFA
            for (let stateKey in dfa.states) {
                let statesSet = dfa.states[stateKey];
                if ([...statesSet].some((state) => nfa.acceptStates.includes(state))) {
                dfa.acceptStates.add(stateKey);
                }
            }

            return dfa;
        }

        // Example NFA
        // const nfa = {
        //     states: ["q0", "q1", "q2"],
        //     alphabet: ["0", "1"],
        //     transitions: {
        //         q0: { 0: ["q0", "q1"], 1: ["q0"] },
        //         q1: { 1: ["q2"] },
        //         q2: {},
        // },
        //     startState: "q0",
        //     acceptStates: ["q2"],
        // };

        

        // Function to extract alphabet from transitions
        function getAlphabetFromTransitions(transitions) {
            const symbols = new Set();
            Object.values(transitions).forEach(stateTransitions => {
                Object.keys(stateTransitions).forEach(symbol => {
                symbols.add(symbol);
                });
            });
            return Array.from(symbols);
        }

   

        // Now you can use this `nfa` with your conversion function
        // const dfa = convertNFAToDFA(nfa);

</script>


@endsection