@extends('app')

@section('content')
<div class="flex flex-col md:flex-row -mx-3 ">
        <!-- Profile Div -->
        <div class="w-full md:w-1/2 px-3 mb-6 container ">
            <div class="p-4 border border-gray-300 bg-white rounded-lg text-center ">
                <h2 class="text-2xl font-semibold text-gray-700 float-left font-serif ">User Information</h2>
                <div class="flex flex-col items-center mt-16">
                    <!-- <img src="" alt="User Image" class="w-64 h-64 bg-gray-300 rounded-full"> -->
                    <img src="{{ asset('storage/' . $users->photo) }}" alt="Photo" class="w-64 h-64 bg-gray-300 rounded-full border-4 border-purple-600">
                </div>
            
            <!-- User Information Div -->
 
                <!-- <h2 class="text-2xl font-semibold text-gray-700 text-center"></h2> -->
                 <hr class="mt-4">
                <div class="flex flex-col items-start">


                <div class="mt-4 text-xl font-serif font-bold flex items-center">
                    <span class="inline-block ">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="size-6 text-blue-600">
                            <path fill-rule="evenodd" d="M4.5 3.75a3 3 0 0 0-3 3v10.5a3 3 0 0 0 3 3h15a3 3 0 0 0 3-3V6.75a3 3 0 0 0-3-3h-15Zm4.125 3a2.25 2.25 0 1 0 0 4.5 2.25 2.25 0 0 0 0-4.5Zm-3.873 8.703a4.126 4.126 0 0 1 7.746 0 .75.75 0 0 1-.351.92 7.47 7.47 0 0 1-3.522.877 7.47 7.47 0 0 1-3.522-.877.75.75 0 0 1-.351-.92ZM15 8.25a.75.75 0 0 0 0 1.5h3.75a.75.75 0 0 0 0-1.5H15ZM14.25 12a.75.75 0 0 1 .75-.75h3.75a.75.75 0 0 1 0 1.5H15a.75.75 0 0 1-.75-.75Zm.75 2.25a.75.75 0 0 0 0 1.5h3.75a.75.75 0 0 0 0-1.5H15Z" clip-rule="evenodd" />
                            </svg>
                        </span>
                    <label for="text" class=" mr-4 ml-2 font-mono underline">
                        ID:
                    </label>
                    <span class="font-mono">
                        {{ $users->id }}
                    </span>
                </div>
                <div class="mt-4 text-xl font-serif font-bold flex items-center">
                    <span class="inline-block ">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="size-6 text-blue-600">
                        <path fill-rule="evenodd" d="M18.685 19.097A9.723 9.723 0 0 0 21.75 12c0-5.385-4.365-9.75-9.75-9.75S2.25 6.615 2.25 12a9.723 9.723 0 0 0 3.065 7.097A9.716 9.716 0 0 0 12 21.75a9.716 9.716 0 0 0 6.685-2.653Zm-12.54-1.285A7.486 7.486 0 0 1 12 15a7.486 7.486 0 0 1 5.855 2.812A8.224 8.224 0 0 1 12 20.25a8.224 8.224 0 0 1-5.855-2.438ZM15.75 9a3.75 3.75 0 1 1-7.5 0 3.75 3.75 0 0 1 7.5 0Z" clip-rule="evenodd" />
                        </svg>

                        </span>
                    <label for="text" class=" mr-4 ml-2 font-mono underline">
                        Name:
                    </label>
                    <span class="font-mono">
                        {{ $users->name }}
                    </span>
                    
                </div>
                <div class="mt-4 text-xl font-serif font-bold flex items-center">
                    <span class="inline-block ">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" class="size-6 text-blue-600">
                        <path d="M1.5 8.67v8.58a3 3 0 0 0 3 3h15a3 3 0 0 0 3-3V8.67l-8.928 5.493a3 3 0 0 1-3.144 0L1.5 8.67Z" />
                        <path d="M22.5 6.908V6.75a3 3 0 0 0-3-3h-15a3 3 0 0 0-3 3v.158l9.714 5.978a1.5 1.5 0 0 0 1.572 0L22.5 6.908Z" />
                        </svg>

                        </span>
                    <label for="text" class=" mr-4 ml-2 font-mono underline">
                        Email:
                    </label>
                    <span class="font-mono">
                        {{ $users->email }}
                    </span>
                </div>
                <div class="mt-4 text-md font-medium flex items-end w-full justify-end ">
                    <a href="{{route('logout.custom')}}" class=" hover:bg-blue-700 text-blue-900 hover:text-teal-50 border-2 border-slate-500 rounded-full  px-4  py-2">
                        Logout
                    </a>
                </div>
                
                </div>
                 
     
            </div>
        </div>

        
        <!-- History Div -->
        <div class="w-full md:w-1/2 px-3 mb-6 container ">
            <div class="p-4 border border-gray-300 bg-white rounded-lg h-[564px]  overflow-y-scroll">
                <h2 class="text-2xl font-semibold text-gray-700 text-center mb-10 font-serif">History of Using Function</h2>
                <hr>
                @if($history->isEmpty())
                    <p class="w-full h-full text-center pt-10 text-xl font-bold text-slate-400">No history found for this user.</p>
                @else
                    <ul>
                        @foreach($history as $histories)
                            <li> <span class="text-lg font-medium">{{ $histories->detail }}</span>  <span class="text-slate-600 text-sm font-semibold">{{ $histories->created_at->diffForHumans()}}</span> </li>
                        @endforeach
                    </ul>
                @endif
                <!-- <ul class="mt-3 text-xl">
                    <li class="mb-4">Design FA - 01/01/2023 - Successful</li>
                    <li class="mb-4">Test FA - 02/01/2023 - Deterministic</li>
                    <li class="mb-4">Test String - 03/01/2023 - String Accepted</li>
                    <li class="mb-4">Minimize DFA - 04/01/2023 - Successful</li>
                </ul> -->
            </div>
        </div>
    </div>
    

    <script>
        

    </script>

@endsection






