<?php

use App\Http\Controllers\HistoryController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AuthController;



//____________________________Admin
Route::get('/team', function () {
    return view('team');
})->name('admin.team');
Route::get('/tutorial', function () {
    return view('tutorial');
})->name('admin.tutorial');


Route::get('/', function () {
    return view('automata');
});

//_____________________________Automata

Route::get('/isDFA', function () {
    return view('fa.isDFA');
})->name('fa.isDFA');
Route::get('/isStringAccepted', function () {
    return view('fa.isStringAccepted');
})->name('fa.isStringAccepted');
Route::get('/minimizeDFA', function () {
    return view('fa.minimizeDFA');
})->name('fa.minimizeDFA');
Route::get('/NFAtoDFA', function () {
    return view('fa.NFAtoDFA');
})->name('fa.NFAtoDFA');

//_____________________________User

Route::get('/profile', [UserController::class, 'index'])->name('users.profile');
Route::get('/automata', function () {
    return view('automata');
})->name('users.automata');
Route::get('/login', function () {
    return view('login');
})->name('users.login');
Route::get('/register', function () {
    return view('register');
})->name('users.register');

//_____________________________Auth

Route::post('/customLogin', [AuthController::class, 'customLogin'])->name('login.custom');
Route::post('/customRegistration', [AuthController::class, 'customRegistration'])->name('register.custom');
Route::get('/signOut', [AuthController::class, 'signOut'])->name('logout.custom');


//____________________________History

Route::get('/isDFAHistory', [HistoryController::class, 'isDFAHistory'])->name('history.isDFA');
Route::get('/NFA2DFAHistory', [HistoryController::class, 'NFA2DFAHistory'])->name('history.NFA2DFA');
Route::get('/stringIsAcceptedHistory', [HistoryController::class, 'stringIsAcceptedHistory'])->name('history.stringIsAccepted');
Route::get('/minimizeDFAHistory', [HistoryController::class, 'minimizeDFAHistory'])->name('history.minimizeDFA');


